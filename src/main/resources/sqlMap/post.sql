#sql("findPostTypeList")
select * from fly_post_type where 1=1
  #if(is_new??) AND is_new =#para(is_new)
  #end
  #if(status??) AND status =#para(status)
  #end
  order by order_num desc
#end


#sql("findMyPostList")
select p.*,pt.name as type_name,u.nickname,u.id as user_id,u.approve,ls.name as levelName,
COALESCE(tt.count,0) as replayCount from fly_post p 
inner join fly_post_type pt on p.type_id = pt.id
#if(sticky_post??) AND p.sticky_post =#para(sticky_post)
#end
inner join fly_user u on u.id = p.user_id
inner join fly_user_sign us on u.id = us.user_id
inner join fly_level_set ls on ls.id = us.level
left join (select count(*) as count,p.user_id,p.id as post_id from fly_replay_post rp,fly_post p  where rp.post_id = p.id 
group by p.user_id, p.id) as tt
on tt.post_id = p.id
where u.id = #para(user_id)
#end

#sql("findPostListCollectionAll")
select p.title,pc.post_id,pc.c_time from fly_post_collection pc inner join fly_post p on p.id = pc.post_id
where pc.user_id = #para(user_id) order by pc.c_time desc
#end


#sql("findAllPostList")
select p.*,pt.name as type_name,u.nickname,u.id as user_id,u.avatar,u.approve,ls.name as levelName,
COALESCE(tt.count,0) as replayCount from fly_post p 
inner join fly_post_type pt on p.type_id = pt.id
#if(sticky_post??) AND p.sticky_post =#para(sticky_post)
#end
inner join fly_user u on u.id = p.user_id
inner join fly_user_sign us on u.id = us.user_id
inner join fly_level_set ls on ls.id = us.level
left join (select count(*) as count,p.user_id,p.id as post_id from fly_replay_post rp,fly_post p  where rp.post_id = p.id group by p.user_id, p.id) as tt
on tt.post_id = p.id
order by p.c_time desc 
limit 0,20
#end

#sql("findGoodPostList")
select p.*,pt.name as type_name,u.nickname,u.id as user_id,u.avatar,u.approve,ls.name as levelName,
COALESCE(tt.count,0) as replayCount from fly_post p 
inner join fly_post_type pt on p.type_id = pt.id
#if(sticky_post??) AND p.sticky_post =#para(sticky_post)
#end
inner join fly_user u on u.id = p.user_id
inner join fly_user_sign us on u.id = us.user_id
inner join fly_level_set ls on ls.id = us.level
left join (select count(*) as count,p.user_id,p.id as post_id from fly_replay_post rp,fly_post p  where rp.post_id = p.id group by p.user_id, p.id) as tt
on tt.post_id = p.id
order by p.c_time desc
limit 0,6
#end

#sql("findPostListByAccept")
select p.*,pt.name as type_name,u.nickname,u.id as user_id,u.approve,ls.name as levelName,
COALESCE(tt.count,0) as replayCount from fly_post p 
inner join fly_post_type pt on p.type_id = pt.id
inner join fly_user u on u.id = p.user_id
inner join fly_user_sign us on u.id = us.user_id
inner join fly_level_set ls on ls.id = us.level
left join (select count(*) as count,p.user_id,p.id as post_id from fly_replay_post rp,fly_post p  where rp.post_id = p.id group by p.user_id, p.id) as tt
on tt.post_id = p.id where 1 = 1 AND p.accept =#para(accept)
order by p.c_time desc 
#end

#sql("findPostListBySolved")
select p.*,pt.name as type_name,u.nickname,u.id as user_id,u.approve,ls.name as levelName,
COALESCE(tt.count,0) as replayCount from fly_post p 
inner join fly_post_type pt on p.type_id = pt.id
inner join fly_user u on u.id = p.user_id
inner join fly_user_sign us on u.id = us.user_id
inner join fly_level_set ls on ls.id = us.level
left join (select count(*) as count,p.user_id,p.id as post_id from fly_replay_post rp,fly_post p  where rp.post_id = p.id group by p.user_id, p.id) as tt
on tt.post_id = p.id where 1 = 1 AND p.fine_post = #para(fine_post)
order by p.c_time desc 
#end

#sql("findPostListByType")
select p.*,pt.name as type_name,u.nickname,u.id as user_id,u.approve,ls.name as levelName,
COALESCE(tt.count,0) as replayCount from fly_post p 
inner join fly_post_type pt on p.type_id = pt.id
inner join fly_user u on u.id = p.user_id
inner join fly_user_sign us on u.id = us.user_id
inner join fly_level_set ls on ls.id = us.level
left join (select count(*) as count,p.user_id,p.id as post_id from fly_replay_post rp,fly_post p  where rp.post_id = p.id group by p.user_id, p.id) as tt
on tt.post_id = p.id where 1 = 1 AND pt.code = #para(code)
order by p.c_time desc 
#end

#sql("findPostList")
select p.id,p.type_id,p.title,p.content,p.status,p.price,
p.hits,p.comment,p.accept,p.sticky_post,p.fine_post,p.c_time,
pt.name as type_name,u.nickname,u.id as user_id,u.approve,ls.name as levelName,
COALESCE(tt.count,0) as replayCount from fly_post p 
inner join fly_post_type pt on p.type_id = pt.id
inner join fly_user u on u.id = p.user_id
inner join fly_user_sign us on u.id = us.user_id
inner join fly_level_set ls on ls.id = us.level
left join (select count(*) as count,p.user_id,p.id as post_id from fly_replay_post rp,fly_post p  where rp.post_id = p.id group by p.user_id, p.id) as tt
on tt.post_id = p.id
order by p.c_time desc 
#end

#sql("findCountPost")
select count(*) from fly_post
#end

#sql("findPostDetail")
select p.id,p.type_id,p.title,REPLACE(REPLACE(p.content, CHAR(10), '\\n'), CHAR(13), '\\n') as content,p.status,p.price,
p.hits,p.comment,p.accept,p.sticky_post,p.fine_post,p.c_time,
pt.name as type_name,u.nickname,u.avatar,u.id as user_id,u.approve,ls.name as levelName,
COALESCE(tt.count,0) as replayCount from fly_post p 
inner join fly_post_type pt on p.type_id = pt.id
#if(id??) AND p.id =#para(id)
#end
inner join fly_user u on u.id = p.user_id
inner join fly_user_sign us on u.id = us.user_id
inner join fly_level_set ls on ls.id = us.level
left join (select count(*) as count,p.user_id,p.id as post_id from fly_replay_post rp,fly_post p  where rp.post_id = p.id group by p.user_id, p.id) as tt
on tt.post_id = p.id
#end

#sql("findPostDetail2")
select p.id,p.type_id,p.title,p.content,p.status,p.price,
p.hits,p.comment,p.accept,p.sticky_post,p.fine_post,p.c_time,
pt.name as type_name,u.nickname,u.id as user_id,u.approve,ls.name as levelName,
COALESCE(tt.count,0) as replayCount from fly_post p 
inner join fly_post_type pt on p.type_id = pt.id
#if(id??) AND p.id =#para(id)
#end
inner join fly_user u on u.id = p.user_id
inner join fly_user_sign us on u.id = us.user_id
inner join fly_level_set ls on ls.id = us.level
left join (select count(*) as count,p.user_id,p.id as post_id from fly_replay_post rp,fly_post p  where rp.post_id = p.id group by p.user_id, p.id) as tt
on tt.post_id = p.id
#end



#sql("findUserSignByUserId")
select * from fly_user_sign where 1 = 1
  #if(user_id??) AND user_id =#para(user_id)
  #end
#end

#sql("findLevelSetAll")
select * from fly_level_set where 1 = 1
  #if(id??) AND id =#para(id)
  #end
  order by order_num
#end

#sql("findReplayPostList")
select rp.*,p.user_id,fu.nickname,fu.avatar,fu.approve,rp.replay_time,fus.level_name,rp.replay_user_id
from fly_replay_post rp 
inner join fly_post p on p.id = rp.post_id
inner join fly_user fu ON rp.replay_user_id = fu.id
inner join fly_user_sign fus on fus.user_id = fu.id
where rp.post_id =#para(id)
order by replay_time desc
#end


#sql("findPostListAll")
select p.id,p.type_id,p.title,p.content,p.status,p.price,
p.hits,p.comment,p.accept,p.sticky_post,p.fine_post,p.c_time,
pt.name as type_name,u.nickname,u.id as user_id,u.avatar,u.approve,ls.name as levelName,p.user_id,
COALESCE(tt.count,0) as replayCount from fly_post p 
inner join fly_post_type pt on p.type_id = pt.id
inner join fly_user u on u.id = p.user_id
inner join fly_user_sign us on u.id = us.user_id
inner join fly_level_set ls on ls.id = us.level
left join (select count(*) as count,p.user_id,p.id as post_id from fly_replay_post rp,fly_post p  where rp.post_id = p.id group by p.user_id, p.id) as tt
on tt.post_id = p.id where 1 = 1
#if(code??)
and pt.code = #para(code)
#end
#if(fine_post??)
and p.fine_post = #para(fine_post)
#end
#if(fine_post??)
and p.fine_post = #para(fine_post)
#end
#if(accept??)
and p.accept =#para(accept)
#end
order by p.c_time desc
#end
