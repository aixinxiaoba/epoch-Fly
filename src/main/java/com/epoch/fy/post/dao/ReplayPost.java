package com.epoch.fy.post.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;
/**
 * 
* <p>Title: ReplayPost.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
@ModelBind(table = "fly_replay_post")
public class ReplayPost extends BaseModel<ReplayPost> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final ReplayPost dao = new ReplayPost();
	
	public static final String ID = "ID"; // 主键id
	public static final String POST_ID = "POST_ID";
	public static final String REPLAY_TIME = "REPLAY_TIME";
	public static final String REPLAY_TEXT = "REPLAY_TEXT";
	public static final String REPLAY_USER_ID = "REPLAY_USER_ID";
	public static final String PRAISE_COUNT = "PRAISE_COUNT";
	public static final String ACCEPT = "ACCEPT";
}
