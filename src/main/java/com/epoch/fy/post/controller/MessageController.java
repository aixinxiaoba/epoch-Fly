package com.epoch.fy.post.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.epoch.base.annotation.ControllerBind;
import com.epoch.base.controller.BaseController;
import com.epoch.fy.post.dao.MyMsg;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;

/**
 * 
* <p>Title: MessageController.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
@ControllerBind(controllerKey = "/message")
public class MessageController extends BaseController{
	
	public void find() {
		Integer user_id = getUserId();
		Kv param = Kv.create();
		param.put("user_id", user_id);
		SqlPara sqlPara = Db.getSqlPara("fly.findMyMsgList", param);
		List<MyMsg> list = MyMsg.dao.find(sqlPara);
		renderSuccess("",Ret.by("rows", list));
	}
	
	public void remove() {
		String id = getPara("id");
		if(StringUtils.isNoneBlank(id)) {
			MyMsg my = MyMsg.dao.findById(id);
			my.delete();
		}else{
			Kv param = Kv.create();
			param.put("user_id", getUserId());
			SqlPara sqlPara = Db.getSqlPara("fly.findMyMsgList", param);
			List<MyMsg> list = MyMsg.dao.find(sqlPara);
			for (MyMsg myMsg : list) {
				myMsg.delete();
			}
		}
		renderSuccess("",Ret.by("status", 0));
	}
}
