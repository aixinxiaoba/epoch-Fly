package com.epoch.fy.post.controller;

import java.util.List;

import com.epoch.base.annotation.ControllerBind;
import com.epoch.base.controller.BaseController;
import com.epoch.base.interceptor.FrontAuthInterceptor;
import com.epoch.fy.post.dao.Post;
import com.epoch.fy.post.dao.PostType;
import com.epoch.fy.post.service.PostService;
import com.epoch.fy.user.dao.User;
import com.epoch.fy.user.dao.UserSign;
import com.epoch.fy.user.service.UserService;
import com.jfinal.aop.Before;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
/**
 * 
* <p>Title: PostController.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
@ControllerBind(controllerKey = "/post")
@SuppressWarnings("unchecked")
public class PostController extends BaseController{
	
	@Before(FrontAuthInterceptor.class)
	public void newPost() {
		Kv param = Kv.create();
		param.put("is_new", "1");
		param.put("status", "1");
		SqlPara sqlPara = Db.getSqlPara("fly.findPostTypeList", param);
		List<PostType> list = PostType.dao.find(sqlPara);
		setAttr("list", list);
		render("/community/post/new.html");
	}
	
	@Before(FrontAuthInterceptor.class)
	public void listMyPost() {
		Kv param = commonParams();
		param.put("user_id", getUserId());
		SqlPara sqlPara = Db.getSqlPara("fly.findMyPostList", param);
		Page<Post> myList = Post.dao.paginate(sqlPara, param);
		renderJson(myList);
	}
	
	@Before(FrontAuthInterceptor.class)
	public void listMyCollection() {
		Kv param = commonPostParams();
		param.set("user_id",getUserId());
		SqlPara sqlPara = Db.getSqlPara("fly.findPostListCollectionAll", param);
        Page<Post> postList= Post.dao.paginate(sqlPara, param);
		renderJson(postList);
	}
	
	@Before(FrontAuthInterceptor.class)
	public void save() {
		String type = getPara("class");
		String title = getPara("title");
		String content = getPara("content");
		String price = getPara("experience");
		price = price == null ? "0":price;
		Integer p = Integer.parseInt(price);
		UserSign userSign = UserService.me.getUserSign(getUserId());
		Integer gold = userSign.getInt(User.GOLD);
		if(gold < p) {
			renderFail("发布失败，金币不足", Ret.by("button", "ok"));
		}else {
			Post post = new Post();
			post.set(Post.TITLE, title);
			post.set(Post.CONTENT, content);
			post.set(Post.TYPE_ID, type);
			post.set(Post.PRICE, price);
			post.set(Post.USER_ID, getUserId());
			post.set(Post.HITS, 0);
			post.set(Post.ACCEPT, 0);
			post.set(Post.STICKY_POST, 0);
			post.set(Post.STATUS, 0);
			post.save();
			userSign.set(User.GOLD, gold-p);
			userSign.update();
			PostService.me.setMsg(content, getUserId(),post.getInt(Post.ID));
			PostType postType = PostType.dao.findById(Integer.parseInt(type));
			renderSuccess("保存成功",Ret.by("code", postType.getStr(PostType.CODE)));
		}
	}
	
	public void postListData() {
		Kv param = commonPostParams();
		param.put("code", getCode(getPara("type")));
		SqlPara sqlPara = Db.getSqlPara("fly.findPostListAll", param);
        Page<Post> postList= Post.dao.paginate(sqlPara, param);
		renderJson(postList);
	}

}
