package com.epoch.fy.post.service;

import java.util.ArrayList;
import java.util.List;

import com.epoch.fy.post.dao.MyMsg;
import com.epoch.fy.user.dao.User;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
/**
 * 
* <p>Title: PostService.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
public class PostService {
	
	public static final PostService me = new PostService();
	
	public void setMsg(String content,Integer user_id,Integer post_id) {
		String[] str = content.split(" ");
		List<String> userNameList = new ArrayList<String>();
		for (int i = 0; i < str.length; i++) {
			String string = str[i];
			if(string.startsWith("@") && string.length()>3 && string.length()< 11) {
				userNameList.add(string.replaceFirst("@", ""));
			}else {
				break;
			}
		}
		for (String string : userNameList) {
			Kv param = Kv.create();
			param.set("nickname",string);
			SqlPara sqlPara = Db.getSqlPara("fly.findUserByNickName", param);
			User user = User.dao.findFirst(sqlPara);
			if(null != user) {
				MyMsg msg = new MyMsg();
				msg.set(MyMsg.USER_ID, user.getInt(User.ID));
				msg.set(MyMsg.TO_USER_ID, user_id);
				msg.set(MyMsg.POST_ID, post_id);
				msg.save();
			}
		}
	}
}
