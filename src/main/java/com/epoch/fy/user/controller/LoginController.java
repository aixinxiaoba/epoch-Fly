package com.epoch.fy.user.controller;

import java.util.List;

import com.epoch.base.annotation.ControllerBind;
import com.epoch.base.controller.BaseController;
import com.epoch.base.interceptor.UserAuthInterceptor;
import com.epoch.fy.post.dao.Post;
import com.epoch.fy.user.dao.Session;
import com.epoch.fy.user.dao.User;
import com.epoch.fy.user.service.UserService;
import com.epoch.fy.user.validator.RegisterValidator;
import com.jfinal.aop.Before;
import com.jfinal.kit.HashKit;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import com.jfinal.plugin.ehcache.CacheKit;
/**
 * 
* <p>Title: LoginController.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
@ControllerBind(controllerKey = "/")
public class LoginController extends BaseController{
	
	public void index() {
		redirect("/community");
	}
	
	
	public void community() {
		Kv param = Kv.create();
		param.put("type", "");
		SqlPara sqlPara = Db.getSqlPara("fly.findAllPostList", param);
		List<Post> postList = Post.dao.find(sqlPara);
		param.put("sticky_post", 1);
		sqlPara = Db.getSqlPara("fly.findGoodPostList", param);
		List<Post> postStickyList = Post.dao.find(sqlPara);
		sqlPara = Db.getSqlPara("fly.findPostPlayWeekTop", param);
		List<Post> weekReplayList = Post.dao.find(sqlPara);
		sqlPara = Db.getSqlPara("fly.findPostPlayWeekHeat", param);
		List<Post> weekHeatReplayList = Post.dao.find(sqlPara);
		setAttr("postStickyList", postStickyList);
		setAttr("weekHeatReplayList", weekHeatReplayList);
		setAttr("postList", postList);
		setAttr("allCls", "layui-this");
		setAttr("weekReplayList",weekReplayList);
		setAttr("count", postStickyList.size());
		setAttr("index", 1);
		render("/community/index.html");
	}
	
	public void login() {
		render("/community/login.html");
	}
	
	public void register() {
		setAttr("curId", 1);
		render("/community/login.html");
	}
	
	/**
	 * 退出登录
	 */
	public void logout() {
		String sessionId = getCookie(UserService.sessionIdName);
		if (sessionId != null) {
			CacheKit.remove(UserService.loginAccountCacheName, sessionId);
			Session.dao.deleteById(sessionId);
		}
		removeCookie(UserService.sessionIdName);
		redirect("/community");
	}

	public void doLogin() {
		Ret ret = UserService.me.login(getPara("account"), getPara("pass"));
		if (ret.isOk()) {
			String sessionId = ret.getStr(UserService.sessionIdName);
			setCookie(UserService.sessionIdName, sessionId, -1, true);
			setAttr(UserService.loginAccountCacheName, ret.get(UserService.loginAccountCacheName));
			renderSuccess("登录成功", Ret.by("goUrl", "/my/my"));
		}else {
			renderFail("用户名密码校验失败,请核对后重新登录!");
		}
		
	}
	

	@Before(RegisterValidator.class)
	public void doRegister() {
		String email = getPara("email");
		String username = getPara("username");
		String pass = getPara("pass");
		String repass = getPara("repass");
		if(!pass.equalsIgnoreCase(repass)) {
			renderFail("密码输入不一致请重新输入");
		}else if(UserService.me.vlidateRegisn(email, username)){
			renderFail("邮箱或者昵称已存在，请重新填写");
		}else if(username.length()<2 || username.length()>10){
			renderFail("昵称必须是2到10个字符或者汉字");
		}else {
			User user = new User();
			user.set(User.EMAIL, email);
			user.set(User.NICKNAME, username);
			String salt = StrKit.getRandomUUID();
			String hashedPass = HashKit.sha256(salt + pass);
			user.set(User.SALT, salt);
			user.set(User.PASSWORD, hashedPass);
			user.set(User.POST_STATUS, 1);
			user.set(User.AVATAR, UserService.USER_AVASTAR);
			user.save();
			renderSuccess("登录成功", Ret.by("goUrl", "/login"));
		}
	}
	
	public void captcha() {
		renderCaptcha();
	}
	
	@Before(UserAuthInterceptor.class)
	public void update() {
		String id = getPara("id");
		String sex = getPara("sex");
		String city = getPara("city");
		String sign = getPara("sign");
		User user = User.dao.findById(id);
		user.set(User.GENDER, sex);
		user.set(User.SIGNATURE, sign);
		user.set(User.CITY, city);
		user.update();
		renderSuccess("保存成功",Ret.by("button", "ok"));
	}
	
}
