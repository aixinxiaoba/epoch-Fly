package com.epoch.fy.user.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;
import com.jfinal.kit.Ret;
/**
 * 
* <p>Title: User.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
@ModelBind(table = "fly_user")
public class User extends BaseModel<User> {

	private static final long serialVersionUID = 1L;
	
	public static final User dao = new User();
	
	public static final String ID = "ID"; // 主键id
	public static final String NICKNAME = "NICKNAME"; // 
	public static final String PASSWORD = "PASSWORD"; //
	public static final String SALT = "SALT"; //
	public static final String EMAIL = "EMAIL"; // 
	public static final String EMAIL_STATUS = "EMAIL_STATUS"; //
	public static final String MOBILE = "MOBILE"; //
	public static final String MOBILE_STATUS = "MOBILE_STATUS"; //
	public static final String POINT = "POINT"; //
	public static final String LEVEL = "LEVEL"; //
	public static final String GENDER = "GENDER"; //
	public static final String BIRTHDAY = "BIRTHDAY"; //
	public static final String STATUS = "STATUS"; //
	public static final String POST_STATUS = "POST_STATUS"; //
	public static final String AVATAR = "AVATAR"; //
	public static final String SIGNATURE = "SIGNATURE";
	public static final String CITY = "CITY";
	public static final String GOLD = "GOLD";
	
	public User removeSensitiveInfo() {
		remove("password", "salt");
		return this;
	}
	
	public Ret isLocked() {
		if (this.getInt(User.STATUS) == 0) {
			return Ret.fail("msg", "账号已被锁定");
		}
		if (this.getInt(User.STATUS) == 2) {
			return Ret.fail("msg", "账号未激活，请先激活账号");
		}
		return Ret.ok();
	}
	
	
	
	

}
