package com.epoch.fy.user.validator;

import com.epoch.base.validator.BaseValidator;
import com.jfinal.core.Controller;
/**
 * 
* <p>Title: RegisterValidator.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
public class RegisterValidator extends BaseValidator{

	@Override
	protected void validate(Controller c) {
		validateRequiredString("email", "message", "请输入用户名");
		validateRequiredString("username", "message", "请输入昵称");
		validateRequiredString("pass", "message", "请输入密码");
		validateRequiredString("repass", "message", "请输入确认密码");
		validateEmail("email", "message", "请输入正确的Email");
	}

	@Override
	protected void handleError(Controller c) {

	}

}
