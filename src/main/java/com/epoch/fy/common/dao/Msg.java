package com.epoch.fy.common.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;
/**
 * 
* <p>Title: Msg.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
@ModelBind(table = "fly_msg")
public class Msg extends BaseModel<Msg> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final Msg dao = new Msg();
	
	public static final String ID = "ID"; // 主键id
	public static final String USER_ID = "USER_ID";
	public static final String PHONE = "PHONE";
	public static final String CODE = "CODE";
	public static final String MSG = "MSG";
	public static final String EXPIRE_DATE = "EXPIRE_DATE";
}
