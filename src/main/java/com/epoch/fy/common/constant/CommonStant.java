package com.epoch.fy.common.constant;
/**
 * 
* <p>Title: CommonStant.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
public class CommonStant {
	
	public static final String BASE_PATH = "/static/file/";
	
	public static String getEpochPath(String fileName) {
		return BASE_PATH+fileName;
	}
}
