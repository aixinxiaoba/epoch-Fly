package com.epoch.base.controller;

import java.util.List;

import com.epoch.fy.post.dao.Post;
import com.epoch.fy.user.dao.User;
import com.epoch.fy.user.service.UserService;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.interceptor.NotAction;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
/**
 * 
* <p>Title: BaseController.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
public class BaseController extends Controller {

	private User user = null;

	@Before(NotAction.class)
	public User getLoginAccount() {
		if (user == null) {
			user = getAttr(UserService.loginAccountCacheName);
			if (user != null && user.isLocked().isFail()) {
				throw new IllegalStateException("当前用户状态不允许登录");
			}
		}
		return user;
	}
	
	@Before(NotAction.class)
	public Integer getUserId() {
		if(isLogin()) {
			return user.getInt(User.ID);
		}
		return null;
	}

	@Before(NotAction.class)
	public boolean isLogin() {
		return getLoginAccount() != null;
	}

	@Before(NotAction.class)
	public boolean notLogin() {
		return !isLogin();
	}

	protected void renderFail(String message,Ret ret) {
		if(null != ret) {
			renderJson(Ret.fail().set("message", message).set(ret));
		}else {
			renderJson(Ret.fail().set("message", message));
		}
		
	}
	
	protected Kv commonPostParams() {
		Kv param = Kv.create();
		//获取页码数
        Integer pageSize = 10;
        Integer pageNumber = getParaToInt("page",1);
        param.set("limit", pageSize);
        param.set("page", pageNumber);
        setAttr("page", pageNumber);
        String curId = getPara("curid");
        setAttr("curid", curId);
        String subtype = getPara("subtype");
        if(null !=subtype && subtype.equalsIgnoreCase("unsolved")) {
        	param.put("accept", "0");
		}
        if(null !=subtype && subtype.equalsIgnoreCase("solved")) {
        	param.put("accept", "1");
		}
        if(null !=subtype && subtype.equalsIgnoreCase("wonderful")) {
        	param.put("fine_post", "1");
		}
        return param;
	}
	
	protected void renderFail(String message) {
		renderJson(Ret.fail().set("message", message));
	}
	
	protected void renderSuccess(String message) {
		renderJson(Ret.ok().set("message", message));
	}
	
	protected void renderSuccess(String message,Ret ret) {
		if(null != ret) {
			renderJson(Ret.ok().set("message", message).set(ret));
		}else {
			renderJson(Ret.ok().set("message", message));
		}
		
	}
	
	protected Kv commonParams() {
		Kv param = Kv.create();
		//获取页码数
        Integer pageSize = getParaToInt("limit",10);
        Integer pageNumber = getParaToInt("page",1);
        param.set("limit", pageSize);
        param.set("page", pageNumber);
        param.set("sortName", getPara("sortName", null));
        param.set("sortOrder", getPara("sortOrder", null));
        return param;
	}
	
	protected void renderPost(Kv param) {
		setAttr("type", param.getStr("type"));
		setAttr(param.getStr("type")+"Cls", "layui-this");
		SqlPara sqlPara = Db.getSqlPara("fly.findPostPlayWeekTop", param);
		List<Post> weekReplayList = Post.dao.find(sqlPara);
		sqlPara = Db.getSqlPara("fly.findPostPlayWeekHeat", param);
		List<Post> weekHeatReplayList = Post.dao.find(sqlPara);
		setAttr("weekReplayList",weekReplayList);
		setAttr("weekHeatReplayList", weekHeatReplayList);
		render("/community/page.html");
	}
	
	protected String getCode(String type) {
		if(type.equalsIgnoreCase("all")) {
			return null;
		}
		return type;
	}
	
	public User getUser(Integer userId) {
		Kv param = Kv.create();
		param.put("user_id", userId);
		SqlPara sqlPara = Db.getSqlPara("fly.findUserByUserId", param);
		User user = User.dao.findFirst(sqlPara);
		return user;
	}
	
	public User getUser() {
		Kv param = Kv.create();
		param.put("user_id", getUserId());
		SqlPara sqlPara = Db.getSqlPara("fly.findUserByUserId", param);
		User user = User.dao.findFirst(sqlPara);
		return user;
	}

}
