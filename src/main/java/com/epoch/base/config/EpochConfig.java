package com.epoch.base.config;

import org.beetl.core.GroupTemplate;
import org.beetl.ext.jfinal3.JFinal3BeetlRenderFactory;

import com.epoch.base.annotation.AutoBindRoutes;
import com.epoch.base.db.EpochDataSource;
import com.epoch.base.fun.SetSharedVars;
import com.epoch.base.handler.BaseHandler;
import com.epoch.base.interceptor.LoginSessionInterceptor;
import com.epoch.base.ui.UtilFunctions;
import com.epoch.base.util.Config;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.kit.PropKit;
import com.jfinal.log.Log4jLogFactory;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.template.Engine;
/**
 * 
* <p>Title: EpochConfig.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
public class EpochConfig extends JFinalConfig {
	
	private Routes routes;

	@Override
	public void configConstant(Constants me) {
		PropKit.use("epoch-data.properties");
		me.setDevMode(Config.getToBoolean("config.devMode"));
		me.setEncoding("UTF-8");
		me.setLogFactory(new Log4jLogFactory());
		JFinal3BeetlRenderFactory renderFactory = new JFinal3BeetlRenderFactory();
		renderFactory.config();
		// 配置beetl
		GroupTemplate groupTemplate = renderFactory.groupTemplate;
		groupTemplate.setSharedVars(SetSharedVars.getSharedVars());
		groupTemplate.registerFunctionPackage("global", UtilFunctions.class);
		me.setRenderFactory(renderFactory);
		me.setI18nDefaultBaseName("i18n");
		me.setError401View(Config.getStr("PAGES.401"));
		me.setError403View(Config.getStr("PAGES.403"));
		me.setError404View("/error/404.html");
		me.setError404View("/error/500.html");
		me.setMaxPostSize(PropKit.getInt("config.upload.fileSizeLimit", Integer.valueOf(1024 * 1024)).intValue());
		me.setBaseUploadPath(PropKit.get("config.upload.basePath", "upload"));
	}

	@Override
	public void configRoute(Routes me) {
		this.routes=me;
		me.add(new AutoBindRoutes());
	}

	@Override
	public void configEngine(Engine me) {
		
	}

	@Override
	public void configPlugin(Plugins me) {
		EpochDataSource.initDataSource(me);
		me.add(new EhCachePlugin());
	}

	@Override
	public void configInterceptor(Interceptors me) {
		me.add(new LoginSessionInterceptor());
	}

	@Override
	public void configHandler(Handlers me) {
		me.add(new ContextPathHandler("basePath"));
		me.add(new BaseHandler());
	}

}
