package com.epoch.base.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.jfinal.plugin.redis.Redis;

import redis.clients.jedis.Jedis;
/**
 * 
* <p>Title: DateUtils.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
public class DateUtils {
	
    public static final String DATEFORMAT_YYYYMM = "yyyyMM";
    public static final String DATEFORMAT_YYYYMMDD = "yyyyMMdd";
    public static final String DATEFORMAT_YYYYMMDDHH = "yyyyMMddHH";
    public static final String DATEFORMAT_YYYYMMDDHHMM = "yyyyMMddHHmm";
    public static final String DATEFORMAT_YYYY_MM_DD = "yyyy-MM-dd";
    public static final String DATEFORMAT_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String DATEFORMAT_YYYYMMDD_HH_MM_SS = "yyyyMMdd HH:mm:ss";
    public static final String DATEFORMAT_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
    
    public final static String DATE_PATTERN = "yyyy-MM-dd";
	
    public static Timestamp getCurrentDate() {
        List<String> timeList;
        Jedis jedis = null;
        try {
            jedis = Redis.use().getJedis();
            //时间戳两个值，一个是当前时间，一个是已经过去的多少秒
            timeList = jedis.time();
            return new Timestamp(Long.valueOf(timeList.get(0) + "000"));
        } catch (Exception e) {
            //LOGGER.error("-##getCurrentDate is error,", e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return new Timestamp(new Date().getTime());
    }
    
    public static String getDateToString(Date date, String format) {
        if (date == null) {
            return null;
        }
        SimpleDateFormat sf = new SimpleDateFormat(format);
        return sf.format(date);
    }
    
    public static String getDefaultDateToString(Date date) {
        SimpleDateFormat sf = new SimpleDateFormat(DATEFORMAT_YYYY_MM_DD);
        return sf.format(date);
    }
    
    public static String getDefaultTimeToString(Date date) {
        SimpleDateFormat sf = new SimpleDateFormat(DATEFORMAT_YYYY_MM_DD_HH_MM_SS);
        return sf.format(date);
    }
    
    public static String format(Date date) {
        return format(date, DATE_PATTERN);
    }
    
    public static String format(Date date, String pattern) {
        if (date != null) {
            SimpleDateFormat df = new SimpleDateFormat(pattern);
            return df.format(date);
        }
        return null;
    }
}
