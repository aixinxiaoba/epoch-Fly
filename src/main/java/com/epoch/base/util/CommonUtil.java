package com.epoch.base.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * 
* <p>Title: CommonUtil.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
public class CommonUtil {
	
	public static boolean isNow(Date date) {
        //当前时间
        Date now = new Date();
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
        //获取今天的日期
        String nowDay = sf.format(now);
        //对比的时间
        String day = sf.format(date);
        return day.equals(nowDay);
    }
	
	
	public static Integer getDifferenceDay(Date date) {
        //当前时间
        Date now = new Date();
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
        //获取今天的日期
        String nowDay = sf.format(now);
        String day = sf.format(date);

        Float nd = new Float(1000 * 24 * 60 * 60);// 一天的毫秒数
        Float diff = Float.valueOf(0);
		try {
			diff = new Float(sf.parse(nowDay).getTime() - sf.parse(day).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
        Float day_ = diff / nd;// 计算差多少天
        return day_.intValue();//返回相差的天数
    }
}
