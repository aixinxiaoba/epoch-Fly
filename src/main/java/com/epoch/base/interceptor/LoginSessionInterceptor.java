package com.epoch.base.interceptor;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.epoch.base.util.CommonUtil;
import com.epoch.fy.user.dao.LevelSet;
import com.epoch.fy.user.dao.User;
import com.epoch.fy.user.dao.UserSign;
import com.epoch.fy.user.service.UserService;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.plugin.ehcache.CacheKit;
/**
 * 
* <p>Title: LoginSessionInterceptor.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
public class LoginSessionInterceptor implements Interceptor{

	@Override
	public void intercept(Invocation inv) {
		User user = null;
		Controller c = inv.getController();
		String sessionId = c.getCookie(UserService.sessionIdName);
		if (sessionId != null) {
			user = UserService.me.getLoginAccountWithSessionId(sessionId);
			if (user == null) {
				String loginIp = getIpAddress(c.getRequest());
				user = UserService.me.loginWithSessionId(sessionId, loginIp);
			}
			if (user != null) {
				// 用户登录账号
				c.setAttr(UserService.loginAccountCacheName, user);
				Integer userId = user.getInt(User.ID);
				UserSign userSign = CacheKit.get(UserService.userSignCacheName, sessionId);
				c.setAttr("experience", 5);
				if(null == userSign) {
					userSign = UserService.me.getUserSign(userId);
					if(null == userSign) {
						userSign = new UserSign();
						userSign.set("sign_count", 0);
						userSign.set("exp", 0);
						userSign.set("gold", 0);
						userSign.set("kiss", 0);
						userSign.set(UserSign.LEVEL, UserService.me.getLevelSetId(0).getInt(LevelSet.ID));
						userSign.set(UserSign.LEVEL_NAME, UserService.me.getLevelSetId(0).getStr(LevelSet.NAME));
						userSign.set(UserSign.LEVEL_IS_VIP, UserService.me.getLevelSetId(0).getStr(LevelSet.IS_VIP));
					}
					CacheKit.put(UserService.userSignCacheName, sessionId, userSign);
				}
				if(null != userSign && null != userSign.get(UserSign.ID)) {
					Date date = userSign.getDate(UserSign.SIGN_TIME);
					int signCount = userSign.getInt(UserSign.SIGN_COUNT);
					int kiss = UserService.me.getGold(signCount);
					if(CommonUtil.isNow(date)) {
						c.setAttr("signed", 1);
					}
					c.setAttr("experience", kiss);
				}
				c.setAttr("userSign", userSign);
			} else {
				c.removeCookie(UserService.sessionIdName); // cookie 登录未成功，证明该 cookie 已经没有用处，删之
			}
		}
		
		inv.invoke();
	}
	
	private String getIpAddress(HttpServletRequest request) {  
        String ip = request.getHeader("x-forwarded-for");  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("WL-Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_CLIENT_IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getRemoteAddr();  
        }  
        return ip;  
    }  

}
