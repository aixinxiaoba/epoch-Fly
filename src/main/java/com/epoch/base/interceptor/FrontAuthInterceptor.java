package com.epoch.base.interceptor;

import com.epoch.fy.user.service.UserService;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.kit.StrKit;
/**
 * 
* <p>Title: FrontAuthInterceptor.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
public class FrontAuthInterceptor implements Interceptor {
	
	public void intercept(Invocation inv) {
		if (inv.getController().getAttr(UserService.loginAccountCacheName) != null) {
			inv.invoke();
		} else {
			String queryString = inv.getController().getRequest().getQueryString();
			if (StrKit.isBlank(queryString)) {
				inv.getController().redirect("/login");
			} else {
				inv.getController().redirect("/login?returnUrl=" + inv.getActionKey() + "?" + queryString);
			}
		}
	}
}
