package com.epoch.base.interceptor;

import com.epoch.fy.user.service.UserService;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.kit.Ret;
/**
 * 
* <p>Title: UserAuthInterceptor.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月24日
 */
public class UserAuthInterceptor implements Interceptor{

	@Override
	public void intercept(Invocation inv) {
		if (inv.getController().getAttr(UserService.loginAccountCacheName) != null) {
			inv.invoke();
		} else {
			renderNotLogin(inv);
		}
	}

	private void renderNotLogin(Invocation inv) {
		inv.getController().renderJson(Ret.fail().set("message", "请先登录").set("code", 886));
	}

}
