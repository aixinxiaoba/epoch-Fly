package com.epoch.base.tag;

import org.beetl.core.Context;
import org.beetl.core.Function;
import org.beetl.core.GroupTemplate;
import org.beetl.core.exception.BeetlException;
import org.beetl.core.exception.ScriptEvalError;
import org.beetl.core.resource.StringTemplateResourceLoader;

import java.io.StringWriter;
import java.util.Map;

public class EvalGlobalValueFunction implements Function {

	private StringTemplateResourceLoader rs = new StringTemplateResourceLoader();
	public Object call(Object[] arg0, Context ctx) {
		// TODO Auto-generated method stub
		String exp = (String)arg0[0];
		GroupTemplate gt = ctx.gt;
		StringWriter sw = new StringWriter();
		Map result;
		try {
			result = gt.runScript("DIRECTIVE SAFE_OUTPUT_OPEN ;var _xxx_=("+exp+");", ctx.globalVar,sw, rs);
			return result.get("_xxx_");
		} catch (ScriptEvalError e) {
			throw new BeetlException(BeetlException.ERROR,e.getMessage());
		
		}
		
	}

}
