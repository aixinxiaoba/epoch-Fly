package com.epoch.base.tag;

import org.beetl.core.Context;
import org.beetl.core.Tag;
import org.beetl.core.statement.Statement;
import org.beetl.ext.tag.HTMLTagVarBindingWrapper;

import java.util.LinkedHashMap;

public class HTMLVarBindingTag extends HTMLTagVarBindingWrapper
{

	Tag tag = new HTMLTag();


	@Override
	public void render()
	{
		tag.render();

	}

	public Object[] bindVars(){
		return null;
	}

	public void mapName2Index(LinkedHashMap<String, Integer> map)
	{
		((HTMLTag)tag).setBinds(map);
	}

	public void init(Context ctx, Object[] args, Statement st)
	{
		tag.init(ctx, args, st);
		

	}

}
