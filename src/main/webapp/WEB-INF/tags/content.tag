@var id = tag.id;
@var content = tag.content!"";
@var classValue = tag.classValue!"";
<div id="${id}" class="${classValue}"></div>
<script type="text/javascript">
$(function() {
	var text = '${content}';
	var content = fly.content(text);
	$("#${id}").html(content);
});
</script>