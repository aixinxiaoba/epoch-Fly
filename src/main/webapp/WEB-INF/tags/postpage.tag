@var count = tag.count!"";
@var type = tag.type!"";
@var page = tag.page!"";
<div class="laypage-main" id="page_content">
</div>
<script type="text/javascript">
$(function() {
	var count = '${count}';
	var type = '${type}';
	var page = '${page}';
	initData(type,count,page);
});

function initData(type,count,page){
	$.ajax({
		type : "POST",
		url : $("#basePathId").val() + '/post/postList',
		data:{"type":type,"count":count,"page":page},
		async : false,
		success : function(text) {
			$("#page_content").empty();
			$("#page_content").append(text);
		}
	});
}
</script>