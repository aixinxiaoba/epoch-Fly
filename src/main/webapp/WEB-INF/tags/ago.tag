@var id = tag.id;
@var datas = tag.time!"";
<span id="${id}"></span>
<script type="text/javascript" src="${baseStaticUrl}/layui/lay/modules/util.js"></script>
<script type="text/javascript">
$(function() {
	var time = '${datas}';
	var ago = layui.util.timeAgo(time, 1);
	$("#${id}").text(ago);
});
</script>