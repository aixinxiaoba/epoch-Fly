
layui.define(['laypage', 'fly', 'element', 'flow', 'table'], function(exports){

  var $ = layui.jquery;
  var layer = layui.layer;
  var util = layui.util;
  var laytpl = layui.laytpl;
  var form = layui.form;
  var laypage = layui.laypage;
  var fly = layui.fly;
  var flow = layui.flow;
  var element = layui.element;
  var upload = layui.upload;
  var table = layui.table;

  //我发的贴
  table.render({
    elem: '#LAY_mySendCard'
    ,url: $("#basePathId").val()+'/post/listMyPost'
    ,method: 'post'
    ,cols: [[
       {field: 'title', title: '帖子标题', minWidth: 300, templet: '<div><a href="'+$("#basePathId").val()+'/jie/page/{{ d.id }}/" target="_blank" class="layui-table-link">{{ d.title }}</a></div>'}
      ,{field: 'status', title: '状态', width: 100, align: 'center', templet: function(d){
        if(d.status == 1){
          return '<span style="color: #FF5722;">加精</span>';
        } else if(d.status == -1){
          return '<span style="color: #ccc;">审核中</span>';
        } else {
          return '<span style="color: #999;">正常</span>'
        }
      }}
      ,{field: 'accept', title: '结贴', width: 100, align: 'center', templet: function(d){
        return d.accept >= 0 ? '<span style="color: #5FB878;">已结</span>' : '<span style="color: #ccc;">未结</span>'
      }}
      ,{field: 'c_time', title: '发表时间', width: 120, align: 'center', templet: '<div>{{ layui.util.timeAgo(d.c_time, 1) }}</div>'}
      ,{title: '数据', width: 150, templet: '<div><span style="font-size: 12px;">{{d.hits}}阅/{{d.replaycount}}答</span></div>'}
      ,{title: '操作', width: 100, templet: function(d){
        return d.accept == 0 ? '<a class="layui-btn layui-btn-xs" href="'+$("#basePathId").val()+'/jie/edit/'+ d.id +'" target="_blank">编辑</a>' : ''
      }}
    ]]
    ,page: true
    ,skin: 'line'
  });
  //我收藏的帖
  
  if($('#LAY_myCollectioncard')[0]){
	 table.render({
        elem: '#LAY_myCollectioncard'
    	,url: $("#basePathId").val()+'/post/listMyCollection'
        ,method: 'post'
        ,cols: [[
          {field: 'title', title: '帖子标题', minWidth: 300, templet: '<div><a href="'+$("#basePathId").val()+'/jie/page/{{ d.post_id }}/" target="_blank" class="layui-table-link">{{ d.title }}</a></div>'}
          ,{field: 'c_time', title: '收藏时间', width: 120, align: 'center', templet: '<div>{{ layui.util.timeAgo(d.c_time, 1) }}</div>'}
        ]]
        ,page: true
        ,skin: 'line'
      });
  }

  //显示当前tab
  if(location.hash){
    element.tabChange('user', location.hash.replace(/^#/, ''));
  }

  element.on('tab(user)', function(){
    var othis = $(this), layid = othis.attr('lay-id');
    if(layid){
      location.hash = layid;
    }
  });


  var gather = {}, dom = {
    mine: $('#LAY_mine')
    ,mineview: $('.mine-view')
    ,minemsg: $('#LAY_minemsg')
    ,infobtn: $('#LAY_btninfo')
  };

  //根据ip获取城市
  if($('#L_city').val() === ''){
    $.getScript('http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js', function(){
      $('#L_city').val(remote_ip_info.city||'');
    });
  }

  //上传图片
  if($('.upload-img')[0]){
    layui.use('upload', function(upload){
      var avatarAdd = $('.avatar-add');

      upload.render({
        elem: '.upload-img'
        ,url: $("#basePathId").val()+'/my/uploadHead'
        ,size: 50
        ,before: function(){
          avatarAdd.find('.loading').show();
        }
        ,done: function(res){
          location.reload();
          avatarAdd.find('.loading').hide();
        }
        ,error: function(){
          avatarAdd.find('.loading').hide();
        }
      });
    });
  }

  //提交成功后刷新
  fly.form['set-mine'] = function(data, required){
    layer.msg('修改成功', {
      icon: 1
      ,time: 1000
      ,shade: 0.1
    }, function(){
      location.reload();
    });
  }

  //帐号绑定
  $('.acc-unbind').on('click', function(){
    var othis = $(this), type = othis.attr('type');
    layer.confirm('确定要解绑'+ ({
      qq_id: 'QQ'
      ,weibo_id: '微博'
    })[type] + '吗？', {icon: 5}, function(){
      fly.json('/api/unbind', {
        type: type
      }, function(res){
        if(res.status === 0){
          layer.alert('已成功解绑。', {
            icon: 1
            ,end: function(){
              location.reload();
            }
          });
        } else {
          layer.msg(res.msg);
        }
      });
    });
  });


  //我的消息
  gather.minemsg = function(){
	  
    var delAll = $('#LAY_delallmsg')
    ,tpl = '{{# var len = d.rows.length;\
    if(len === 0){ }}\
      <div class="fly-none">您暂时没有最新消息</div>\
    {{# } else { }}\
      <ul class="mine-msg">\
      {{# for(var i = 0; i < len; i++){ }}\
        <li data-id="{{d.rows[i].id}}">\
          <blockquote class="layui-elem-quote">\
    	<a href="'+$("#basePathId").val()+'/jie/jump?username={{ d.rows[i].nickname}}" target="_blank"><cite>{{ d.rows[i].nickname}}</cite></a>回答了您的求解\
    	<a target="_blank" href="'+$("#basePathId").val()+'/jie/page/{{ d.rows[i].post_id}}"><cite>{{ d.rows[i].title}}</cite></a>\
    	</blockquote>\
          <p><span>{{layui.util.timeAgo(d.rows[i].c_time,1)}}</span><a href="javascript:;" class="layui-btn layui-btn-sm layui-btn-danger fly-delete">删除</a></p>\
        </li>\
      {{# } }}\
      </ul>\
    {{# } }}'
    ,delEnd = function(clear){
    	
      if(clear || dom.minemsg.find('.mine-msg li').length === 0){
        dom.minemsg.html('<div class="fly-none">您暂时没有最新消息</div>');
      }
    }
    
    
    fly.json( $("#basePathId").val()+'/message/find/', {}, function(res){
      var html = laytpl(tpl).render(res);
      dom.minemsg.html(html);
      $("#LAY_delallmsg").hide();
      if(res.rows.length > 0){
    	$("#LAY_delallmsg").show();
        delAll.removeClass('layui-hide');
      }
    });
    
    //阅读后删除
    dom.minemsg.on('click', '.mine-msg li .fly-delete', function(){
      var othis = $(this).parents('li'), id = othis.data('id');
      fly.json($("#basePathId").val()+'/message/remove/', {
        id: id
      }, function(res){
        if(res.status === 0){
          othis.remove();
          delEnd();
        }
      });
    });

    //删除全部
    $('#LAY_delallmsg').on('click', function(){
      var othis = $(this);
      layer.confirm('确定清空吗？', function(index){
        fly.json($("#basePathId").val()+'/message/remove/', {
          all: true
        }, function(res){
          if(res.status === 0){
            layer.close(index);
            othis.addClass('layui-hide');
            delEnd(true);
          }
        });
      });
    });

  };

  dom.minemsg[0] && gather.minemsg();


  //我的产品
  table.render({
    elem: '#LAY_productList'
    ,url: $("#basePathId").val()+'/api/productList'
    ,method: 'post'
    ,cols: [[
      {title: '产品名称', field:'product_name',minWidth: 300}
      ,{field:'authproduct', title: '别名', width: 100}
      ,{title: '属性', width: 150, templet: function(d){
        var arr = [];
        layui.each(d.attr, function(index, item){
          if(item.name){
            arr.push(item.name + '：' + (item.value || ''));
          }
        });
        return arr.join('、')
      }}
      ,{field: 'expiry_time', title: '授权有效期', width: 120, templet: function(d){
        return d.expiry_time ? layui.util.toDateString(d.expiry_time, "yyyy-MM-dd") : '永久';
      }}
      ,{field: 'price', title: '付费金额', width: 120, templet: function(d){
        return '<span style="color: #FF5722;">￥'+ (d.pay_mount || 0) +'</span>';
      }}

      ,{title: '操作', width: 100, templet: function(d){
        if(d.is_expiry == 1){
          return '<a href="#" style="color: #FF5722;" target="_blank">重新授权</a>';
        } else {
          return '<a href="'+$("#basePathId").val()+'/api/download?proId='+ d.id +'" class="layui-table-link" target="_blank">下载</a>';
        }
      }}
    ]]
    ,page: true
    ,text: {
      none: function(){
        var where = {}
        ,alias = layui.data.alias;
        return alias ? '未查询到你的 '+ alias +' 授权记录' : '您还没有任何产品授权'
      }()
    }
    ,done: function(res, curr){
      //无数据
      if(res.data.length === 0 && curr == 1){
        
      }
    }
  })
  exports('user', {});
  
});