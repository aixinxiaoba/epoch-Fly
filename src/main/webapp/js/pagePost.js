$(function() {
	var type = $("#basetypeId").val();
	var curid = $("#baseCuridId").val();
	if(curid.length==0){
		vm.getData(1,type,vm.getType());
		addPager();
	}else{
		vm.getData(1,type,vm.getType(Number(curid)));
		vm.curId = Number(curid);
		addPager();
	}
});
var vm = new Vue({
	el : '#layuiContainer',
	data : {
		postList : [],
		count:0,
		curId:0,
		isHead:false
	},
	methods : {
		getData : function(page,type,subtype) {
			var data = [];
			$.ajax({
				type : "POST",
				url : $("#basePathId").val() + '/post/postListData',
				async : false,
				data:{page:page,type:type,subtype:subtype},
				success : function(r) {
					for (var i = 0; i < r.list.length; i++) {
						var array = {};
						array.id = r.list[i].id;
						array.title = r.list[i].title;
						array.type_name = r.list[i].type_name;
						array.approve = r.list[i].approve == null ? null:"认证信息："+ r.list[i].approve;
						array.levelName = r.list[i].levelname;
                        array.avatar = r.list[i].avatar;
						array.price = r.list[i].price;
						array.nickname = r.list[i].nickname;
						array.accept = r.list[i].accept;
						array.replayCount = r.list[i].replaycount;
						array.sticky_post = r.list[i].sticky_post;
                        array.fine_post = r.list[i].fine_post;
						array.c_time = layui.util.timeAgo(r.list[i].c_time, 1);
						array.skip = "skip('" + r.list[i].id + "')";
						array.headUrl= $("#basePathId").val() +"/my/u/"+r.list[i].user_id; 
						data.push(array);
					}
					vm.count = r.totalRow;
				}
			});
			vm.postList = data;
		},
		getType:function() {
			if(vm.curId == 0){
				return "all";
			}
			if(vm.curId == 1){
				return "unsolved";
			}
			if(vm.curId == 2){
				return "solved";
			}
			if(vm.curId == 3){
				return "wonderful";
			}
			return "all";
		},
		getType:function(value) {
			if(value == 0){
				return "all";
			}
			if(value == 1){
				return "unsolved";
			}
			if(value == 2){
				return "solved";
			}
			if(value == 3){
				return "wonderful";
			}
			return "all";
		}
	},
	watch : {
		'curId':{
			handler:function(val,oldval){
				var type = $("#basetypeId").val();
				var subtype = vm.getType(val);
				vm.getData(1,type,subtype);
				addPager(1);
			}
		}
	}
});

var pagerIsFirst = true;
function addPager() {
	layui.use([ 'laypage', 'laydate', 'form' ], function() {
		var laypage = layui.laypage;
		laypage.render({
			elem : 'course_page',
			count : vm.count,
			limit : 10,
			jump : function(obj, first) {
				var type = $("#basetypeId").val();
				var subtype = vm.getType();;
				pagerIsFirst = first;
				if (!first) {
					vm.getData(obj.curr,type,subtype);
				}
			}
		});
	});
}

function skip(id){
	location.href=$("#basePathId").val()+"/jie/page/"+Number(id);
}

